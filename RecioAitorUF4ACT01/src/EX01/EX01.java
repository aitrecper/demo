package EX01;

import java.util.Scanner;

public class EX01 {
	
	public static void main(){
		int num1, num2, num3;
		
		Scanner input;
		input = new Scanner(System.in);
		System.out.print("Primer numero: ");
		num1=input.nextInt();
		System.out.print("Segundo numero: ");
		num2=input.nextInt();
		num3=num1+num2;
		if (num3%2== 0) {
			System.out.println("Es par");
		}else {
			System.out.println("Es impar");
		}
	}
}
